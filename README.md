# Description
Simple Pinterest image upload.

**App uses SSL so visit localhost/127.0.0.1 via https:// protocol**

## Features
* Pinterest OAuth2 login (login to Pinterest via Facebook is the easiest)
* View board list
* Create board
* Resize image in the browser to reduce request payload size
* Upload an image to a specific board

# Requirements

* Ubuntu 14.04 - I didn`t get the chance to test it anywhere else
* nodejs ^5.3.0
* npm ^3.3.12

# Install

```
npm i
```

# Run

```
npm start
```

... and visit https://127.0.0.1:8443