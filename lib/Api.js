let fs        = require('fs'),
    tmpDir    = require('os').tmpDir(),
    path      = require('path'),
    parse     = require('co-busboy'),
    Pinterest = require('./Pinterest')

class Api {
  /**
   * @description
   * 1. parse incoming form, save temp file
   * 2. make request, return uploaded image`s url to user
   * 3. cleanup temp file
   * @returns {{status: boolean}}
   */
  static * upload() {
    let username = Pinterest.getUsername(this.session),
        parts    = parse(this, {autoFields: true}),
        data     = {
          note: 'Created by SlidesCamp uploader'
        },
        part, tmpFile

    try {
      while (part = yield parts) {
        let hash = Math.random().toString(36).substr(2)
        tmpFile = `${tmpDir}/${hash}`
        let stream = fs.createWriteStream(tmpFile)
        part.pipe(stream)
        yield new Promise(resolve => stream.on('finish', resolve))
      }
      data.board = `${username}/${parts.field.board}`
      data.image = fs.createReadStream(tmpFile)
      let res = yield new Pinterest(this.session).uploadImage(data)
      yield cleanUp()
      this.body = res.data
    } catch (e) {
      console.log(e)
      yield cleanUp()
      return this.body = {status: false}
    }
    function* cleanUp() {
      try {
        return yield fs.unlinkAsync(tmpFile)
      } catch (e) {
        // file doesen`t exist
        return Promise.resolve()
      }
    }
  }

  /**
   * @description merge session data from passport login with user boards
   * fetched from Pinterest
   */
  static * userInfo() {
    try {
      let {data} = yield new Pinterest(this.session).getBoards()
      this.body = Object.assign(this.session.passport.user.profile, {boards: data})
    } catch (e) {
      console.log(e)
      this.body = {}
    }
  }

  /**
   * @see Pinterest#createBoard
   */
  static * createBoard() {
    try {
      let res = yield new Pinterest(this.session).createBoard(this.request.body)
      this.body = {status: true}
    } catch (e) {
      console.log(e)
      this.body = {status: false}
    }
  }

  /**
   * @description in order to make page reload work on nested routes,
   * we need to send index.html on every unmatched request
   */
  static * catchAll() {
    this.set('content-type', 'text/html')
    this.body = fs.createReadStream(path.resolve('public', 'index.html'))
  }

  /**
   * @description if user isn`t authenticated, we either redirect them to login
   * page, or send 401 http status
   * @middleware
   * @param redirect
   * @returns {isAuthenticated}
   */
  static isAuthenticated(redirect) {
    return function* isAuthenticated(next) {
      if (!this.isAuthenticated() && !/auth\//.test(this.url)) {
        if (redirect) {
          this.redirect('/auth/pinterest')
        } else {
          this.status = 401
        }
      } else {
        yield next
      }
    }
  }

  /**
   * @description destroy session
   */
  static * logout() {
    this.session = null
    this.body = {status: true}
  }
}

module.exports = Api
