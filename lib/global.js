/**
 * globally promisifies node filesystem
 * @use require('./global') in the root of the app
 */
let promisifyAll = require('bluebird').promisifyAll,
    fs = require('fs')

promisifyAll(fs)
