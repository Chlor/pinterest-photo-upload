let url     = require('url'),
    nconf   = require('nconf'),
    Request = require('./Request')

const URL = nconf.get('pinterest_url')

class Pinterest {
  /**
   * @description accept passport session and extract access token
   * @throws
   * @param {Object} session - passport session
   */
  constructor(session) {
    let accessToken = Pinterest.getAccessToken(session)
    if (!accessToken) {
      throw new TypeError('Missing accessToken')
    }
    this.accessToken = accessToken
  }

  /**
   * @description in order to upload an image, we need to point to a board,
   * so we fetch board list from Pinterest to allow user to choose, where they
   * want their image posted
   * @returns {Promise}
   */
  * getBoards() {
    return Request.get(this.formatUrl('me', 'boards'))
  }

  /**
   * @description if user has no boards or wants to create new one for upload,
   * this method allows us to do it
   * @param {Object} data - requires `name` key
   * @returns {Promise}
   */
  * createBoard(data) {
    return Request.post(this.formatUrl('boards'), {form: data})
  }

  /**
   * @param {Object} data
   * @model
   * {
   *   board: <username/board>,
   *   image: {stream.Readable},
   *   note: {string}
   * }
   * @returns {Promise}
   */
  * uploadImage(data) {
    return Request.post(this.formatUrl('pins'), {formData: data})
  }

  formatUrl(...path) {
    return `${URL}/${path.join('/')}/?access_token=${this.accessToken}`
  }

  /**
   * @description extract username from session
   * @param {Object} session - passport session
   * @returns {T}
   */
  static getUsername(session) {
    let url = session.passport.user.profile._json.data.url.split('/')
    url.pop()
    return url.pop()
  }

  /**
   * @description extract accessToken
   * @param {Object} - session
   * @returns {*}
   */
  static getAccessToken(session) {
    return session.passport.user.accessToken
  }
}

module.exports = Pinterest
