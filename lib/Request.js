let request = require('request')

/**
 * @description JSON methods tend to throw, so let`s be safe
 * @param body
 * @param resolve
 */
function tryJson(body, resolve) {
  try {
    resolve(JSON.parse(body))
  } catch (e) {
    resolve(body)
  }
}
/**
 * Promisify request module
 */
class Request {
  /**
   *
   * @param {string} url
   * @returns {Promise}
   */
  static get(url) {
    return new Promise((resolve, reject) => {
      request.get(url, (err, res, body) => {
        err ? reject(err) : tryJson(body, resolve)
      })
    })
  }

  /**
   * @param {string} url
   * @param {Object} form - {<form|formData>: {...}}
   * @returns {Promise}
   */
  static post(url, form) {
    return new Promise((resolve, reject) => {
      request.post(url, form, (err, res, body) => {
        err ? reject(err) : tryJson(body, resolve)
      })
    })
  }
}

module.exports = Request
