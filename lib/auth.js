let nconf             = require('nconf'),
    passport          = require('koa-passport'),
    PinterestStrategy = require('passport-pinterest-oauth').OAuth2Strategy

/**
 * Users need to log in with Pinterest OAuth2 endpoint
 */
passport.use(new PinterestStrategy({
    clientID: nconf.get('pinterest_client_id'),
    clientSecret: nconf.get('pinterest_secret'),
    callbackURL: 'https://127.0.0.1:8443/auth/pinterest/callback'
  }, function (accessToken, refreshToken, profile, done) {
    return done(null, {accessToken, refreshToken, profile})
  }
))

passport.serializeUser((user, done) => done(null, user))
passport.deserializeUser((user, done) => done(null, user))

module.exports = passport

