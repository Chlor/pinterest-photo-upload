let nconf      = require('nconf'),
    Router     = require('koa-router'),
    passport   = require('./auth'),
    apiRouter  = Router({prefix: nconf.get('api_prefix')}),
    authRouter = Router({prefix: '/auth'}),
    Api        = require('./Api')

apiRouter
  .post('/image-upload', Api.upload)
  .get('/user-info', Api.userInfo)
  .post('/create-board', Api.createBoard)

authRouter
  .get('/pinterest', function* () {
    yield passport.authenticate('pinterest', {
      scope: [
        'read_public',
        'write_public'
      ]
    })

  })
  .get('/pinterest/callback', function* () {
    yield passport.authenticate('pinterest', {
      failureRedirect: '/auth/pinterest',
      successRedirect: '/upload'
    })
  })
  .get('/logout', Api.logout)

module.exports = {
  auth: authRouter.routes(),
  api: apiRouter.routes()
}
