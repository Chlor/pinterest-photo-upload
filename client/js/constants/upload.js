import Utils from '../lib/Utils'
export default Utils.keyMirror({
  ERROR: null,
  ADD_IMAGE: null,
  SET_BOARD: null,
  CLEAR_FILE: null,
  CLEAR_ERROR: null
})
