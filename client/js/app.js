import React from 'react'
import ReactDOM from 'react-dom'
import {Router} from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import routes from './routes'

const history = createBrowserHistory()

/**
 * Using react router for a project that only has one page might look like an
 * overkill, but it`s a must for any real project, so I decided to use it anyway
 */
ReactDOM.render(<Router history={history}
                        routes={routes}/>, document.querySelector('#mount-point'))
