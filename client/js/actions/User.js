import dispatcher from '../dispatcher'
import constants from '../constants/user'
import Api from '../lib/Api'

export default class UserActions {
  constructor() {
    return UserActions
  }

  /**
   * @description fetch user data
   */
  static async get() {
    try {
      let user = await Api.getUser()
      dispatcher.handleViewAction({
        actionType: constants.SET_USER,
        data: user
      })
    } catch (e) {
      console.log(`Error fetching user ${e}`)
    }
  }

  /**
   * @description after creating a board force the reload of user data
   * in order to refresh the view
   * @param data
   */
  static async createBoard(data) {
    try {
      await Api.createBoard({name: data})
      await UserActions.get()
    } catch (e) {
      console.log(`Error creating a board ${e}`)
    }
  }

  /**
   * @description destroy session, redirect to Pinterest login screen
   */
  static async logout() {
    try {
      let res = await Api.logout()
      if (res) {
        window.location.href = '/auth/pinterest'
      }
    } catch (e) {
      console.log(`Logout error ${e}`)
    }
  }
}
