import dispatcher from '../dispatcher'
import constants from '../constants/upload'
import UploadStore from '../stores/Upload'
import ImageConverter from '../lib/ImageConverter'
import Api from '../lib/Api'

export default class UploadActions {
  constructor() {
    return UploadActions
  }

  /**
   * @description validate image mimeType, set error if necessary
   * @param data
   * @returns {*}
   */
  static async addImage(data) {
    if (!/image\/\w+/.test(data.type)) {
      return dispatcher.handleViewAction({
        actionType: constants.ERROR,
        data: {
          type: 'danger',
          message: 'Bad mime type'
        }
      })
    }

    dispatcher.handleViewAction({
      actionType: constants.ADD_IMAGE,
      data: await ImageConverter.convert(data)
    })
  }

  static clearFile() {
    dispatcher.handleViewAction({
      actionType: constants.CLEAR_FILE
    })
  }

  static clearError() {
    dispatcher.handleViewAction({
      actionType: constants.CLEAR_ERROR
    })
  }

  static setBoard(name) {
    dispatcher.handleViewAction({
      actionType: constants.SET_BOARD,
      data: name
    })
  }

  /**
   * @description
   * 1. get data from store
   * 2. make sure, that the board is chosen
   * 3. set url as message, to that we can display a link with newly uploaded
   * image to the user
   * @returns {*}
   */
  static async upload() {
    let image = UploadStore.getImage(),
        board = UploadStore.getBoard()
    if (!board) {
      return dispatcher.handleViewAction({
        actionType: constants.ERROR,
        data: {message: 'You have to choose a board', type: 'danger'}
      })
    }
    try {
      let res = await Api.postImage(image, board)
      dispatcher.handleViewAction({
        actionType: constants.ERROR,
        data: {message: res.url, type: 'success'}
      })
      dispatcher.handleViewAction({
        actionType: constants.ADD_IMAGE,
        data: null
      })
    } catch(e) {
      dispatcher.handleViewAction({
        actionType: constants.ERROR,
        data: {message: JSON.stringify(e, null, 2), type: 'danger'}
      })
    }
  }
}
