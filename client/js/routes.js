import App from './components/App'
import Header from './components/Header'
import Uploader from './components/Uploader'

const routes = [
  {
    path: '/',
    component: App,
    indexRoute: {component: Uploader},
    childRoutes: [
      {
        path: 'upload',
        component: Uploader
      }
    ]
  }
]

export default routes
