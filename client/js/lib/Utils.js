export default class Utils {
  /**
   * @description mirror keys as values
   * @param obj
   * @returns {*}
   */
  static keyMirror(obj) {
    return Reflect.ownKeys(obj).reduce((p, key) => {
      p[key] = key
      return p
    }, {})
  }
}
