import request from 'superagent'

const URI = '/api/v1'

function finalize(req) {
  req.accept('json')
  return new Promise((resolve, reject) => req.end((err, res) =>
    err ? reject(err.response.error) : resolve(res.body)
  ))
}

/**
 * methods here mirror server Api endpoints
 * @see /lib/Api
 */
export default class Api {
  /**
   * @description superagents attach method doesn`t work particularly well
   * but luckily we can make use of FormData
   * @param file
   * @param board
   */
  static postImage(file, board) {
    let form = new FormData()
    form.append('image', file, file.name)
    form.append('board', board)
    return finalize(request.post(`${URI}/image-upload`).send(form))
  }

  static getUser() {
    return finalize(request.get(`${URI}/user-info`))
  }

  static createBoard(data) {
    return finalize(request.post(`${URI}/create-board`).send(data))
  }

  static logout() {
    return finalize(request.get('/auth/logout'))
  }
}
