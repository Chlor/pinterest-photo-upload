export default class ImageConverter {

  /**
   * @description
   * 1. encode Blob as base64
   * 2. shrink image (create thumbnail)
   * 3. encode as binary
   * 4. copy relevant data and return
   * @main
   * @param {File} img
   * @param {Object} options - constraints
   * @returns {File}
   */
  static async convert(img, options = {maxWidth: 200, maxHeight: 200}) {
    let type      = img.type,
        base64    = await ImageConverter.toBase64(img),
        thumbnail = await ImageConverter.createThumbnail(base64, type, options),
        blob      = ImageConverter.toBlob(thumbnail, type),
        file      = new File([blob], img.name, {type, filename: img.name})
    file.preview = URL.createObjectURL(blob)
    file.filename = img.name
    return file
  }

  /**
   * @description convert blob binary to base64 string
   * @param {Blob} blobImg
   * @returns {Promise}
   */
  static toBase64(blobImg) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader()
      reader.readAsDataURL(blobImg)
      reader.onloadend = () => resolve(reader.result)
      reader.onerror = err => reject(err)
    })
  }

  /**
   * @description encode base64 as binary
   * @param {string} base64Img
   * @param {string} type - mime type
   * @returns {Blob}
   */
  static toBlob(base64Img, type) {
    let binary    = atob(base64Img.split(',').pop()),
        length    = binary.length,
        buffer    = new ArrayBuffer(length),
        byteArray = new Uint8Array(buffer)
    for (let i = 0; i < length; i++) {
      byteArray[i] = binary.charCodeAt(i)
    }
    return new Blob([buffer], {type: type.split(':').pop()})
  }

  /**
   * @description
   * 1. create temp image
   * 2. when it loads, get its dimensions and aspect ratio
   * 3. if they`re within range, exit early
   * 4. detect, if it`s a landscape or a portrait and calc size accordingly
   * 5. draw image to canvas in proper size
   * 6. return image as base64 encoded string
   *
   * @param {string} img - base64 encoded image
   * @param {string} type - mimeType
   * @param {number} maxWidth
   * @param {number} maxHeight
   * @returns {Promise|string}
   */
  static createThumbnail(img, type, {maxWidth, maxHeight}) {
    let tempImg = new Image()
    tempImg.src = img
    return new Promise(resolve => {
      tempImg.addEventListener('load', () => {
        let {width, height} = tempImg,
            ratio = width >= height ? width / height : height / width

        if (width <= maxWidth && height <= maxHeight) {
          return resolve(img)
        }

        let canvas = document.createElement('canvas'),
            ctx    = canvas.getContext('2d'),
            round  = Math.round,
            finalWidth = maxWidth,
            finalHeight = round(maxWidth / ratio)

        if (height > width) {
          finalHeight = maxHeight
          finalWidth = round(maxHeight / ratio)
        }

        canvas.width = finalWidth
        canvas.height = finalHeight
        ctx.drawImage(tempImg, 0, 0, finalWidth, finalHeight)
        resolve(canvas.toDataURL(type))
      })

    })
  }
}
