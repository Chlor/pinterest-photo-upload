import React, {Component} from 'react'
import {Row, Col, Button, Glyphicon, Alert, Panel} from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import Boards from './Boards'
import Api from '../lib/Api'
import UploadStore from '../stores/Upload'
import UploadActions from '../actions/Upload'

function getState() {
  return {
    image: UploadStore.getImage(),
    error: UploadStore.getError()
  }
}

export default class Uploader extends Component {
  constructor(props) {
    super(props)
    this.state = {image: null, error: null}
  }

  componentDidMount() {
    UploadStore.addChangeListener(this._onChange, this)
  }

  componentWillUnmount() {
    UploadStore.removeChangeListener(this._onChange)
  }

  onDrop(image) {
    UploadActions.addImage(image.pop())
  }

  closeAlert() {
    UploadActions.clearError()
  }

  remove() {
    UploadActions.clearFile()
  }

  upload() {
    UploadActions.upload()
  }

  _onChange() {
    this.setState(getState())
  }

  render() {
    let image   = this.state.image,
        error   = this.state.error,
        message = error && error.message

    message = message && (/^https?:\/\//).test(message)
      ? <a href={message}>{message}</a>
      : typeof message === 'object' ? JSON.stringify(message) : message

    return (
      <Row>
        {
          error &&
          <Col sm={12}>
            <Alert bsStyle={error.type}
                   onDismiss={this.closeAlert.bind(this)}>
              {message}
            </Alert>
          </Col>
        }
        <Col sm={4}>
          <Boards user={this.props.user}/>
        </Col>

        <Col sm={8}>
          <Panel bsStyle="primary" header={<h3>Upload</h3>}>
            <Dropzone multiple={false} className={'my-dropzone'}
                      activeClassName={'my-dropzone-active'}
                      onDrop={this.onDrop.bind(this)}>
            <span>Drag&drop or click</span>
            </Dropzone>
            {
              image && <div className="text-center margin-top">
                <Button bsStyle="primary" onClick={this.upload.bind(this)}>
                  <Glyphicon glyph={'upload'}></Glyphicon> Upload
                </Button> <Button bsStyle="danger"
                                  onClick={this.remove.bind(this)}>
                <Glyphicon glyph={'trash'}></Glyphicon> Remove
              </Button>
                <Col sm={12} className="text-center">
                  <h3>{image.name}</h3>
                  <img src={image.preview} alt=""/>
                </Col>
              </div>
            }
          </Panel>
        </Col>
      </Row>
    )
  }
}
