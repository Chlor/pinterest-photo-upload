import React, {Component} from 'react'
import {Input, Button, Panel} from 'react-bootstrap'
import UserStore from '../stores/User'
import UserActions from '../actions/User'
import UploadActions from '../actions/Upload'
import UploadStore from '../stores/Upload'

function getState() {
  return {
    user: UserStore.getUser(),
  }
}

export default class Boards extends Component {
  constructor(props) {
    super(props)
    this.state = getState()
    this.state.boardName = ''
  }

  /**
   * @description call _onChange once to set the state
   */
  componentDidMount() {
    this._onChange()
    UserStore.addChangeListener(this._onChange, this)
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onChange)
  }

  async createBoard() {
    await UserActions.createBoard(this.state.boardName)
    this.setState({boardName: ''})
  }

  /**
   * @description board is needed to upload a file, so we set it from here in
   * the upload data struct
   * @param target
   */
  selectBoard({target}) {
    let value = target.value
    if (value === 'NULL') value = null;
    UploadActions.setBoard(value)
  }

  onBoardNameUpdate({target}) {
    this.setState({boardName: target.value})
  }

  _onChange() {
    this.setState(getState())
  }

  render() {
    let boards    = this.state.user.boards,
        boardList = <Input type="select" onChange={this.selectBoard.bind(this)}>
          <option value="NULL">Choose a board...</option>
          {
            boards.map(({id, name}, index) =>
              <option value={name} key={id}>{name}</option>
            )
          }
        </Input>,
        button    = <Button bsStyle="primary"
                            disabled={!this.state.boardName.length}
                            onClick={this.createBoard.bind(this)}>Create
          Board</Button>
    return (
      <Panel header={<h3>Boards</h3>} bsStyle="primary">
        {
          !boards.length
            ? <p>No boards defined</p>
            : boardList
        }
        <Input type="text" buttonAfter={button}
               value={this.state.boardName}
               onChange={this.onBoardNameUpdate.bind(this)}
               placeholder="Board name"/>
      </Panel>
    )
  }
}
