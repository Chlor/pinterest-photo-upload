import React, {Component} from 'react'
import {Jumbotron, Button} from 'react-bootstrap'
import UserActions from '../actions/User'

export default class Header extends Component {
  constructor(props) {
    super(props)
  }

  async logout() {
    await UserActions.logout()
  }

  render() {
    return (
      <Jumbotron>
        <h1>
          Pinterest photo uploader
        </h1>
        <p>
          Logged in as <b>{this.props.user.displayName}</b> <Button
          onClick={this.logout} bsStyle="primary">
          Logout
        </Button>
        </p>
      </Jumbotron>
    )
  }
}
