import React, {Component} from 'react'
import {Grid} from 'react-bootstrap'
import UserActions from '../actions/User'
import UserStore from '../stores/User'
import UploadActions from '../actions/Upload'
import Header from './Header'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {user: {}}
  }

  /**
   * Load user info once the App is loaded
   */
  async componentDidMount() {
    await UserActions.get()
    this.setState({user: UserStore.getUser()})
  }

  render() {
    return (
      <Grid fluid>
        <Header user={this.state.user}/>
        {
          this.props.children && React.cloneElement(this.props.children, {
            props: this.state
          })
        }
      </Grid>
    )
  }
}
