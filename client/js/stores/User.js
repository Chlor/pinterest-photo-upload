import {EventEmitter} from 'events'
import dispatcher from '../dispatcher'
import constants from '../constants/user'

const DATA   = new Map(),
      EVENTS = new WeakMap()

/**
 * @see stores/Upload
 */
class Internal {
  constructor() {
    return Internal
  }

  static setUser(user) {
    DATA.set('user', user)
  }

  static setInitial() {
    DATA.set('user', {boards: []})
  }
}

Internal.setInitial()

class UserStore extends EventEmitter {

  emitChange() {
    this.emit('change')
  }

  getUser() {
    return DATA.get('user')
  }

  addChangeListener(fn, ctx) {
    EVENTS.set(fn, fn.bind(ctx))
    this.on('change', EVENTS.get(fn))
  }

  removeChangeListener(fn) {
    this.removeListener(EVENTS.get(fn))
    EVENTS.delete(fn)
  }
}

let userStore = new UserStore()

userStore.dispatchToken = dispatcher.register(({action}) => {
  let {actionType, data} = action
  switch (actionType) {
    case constants.SET_USER:
      Internal.setUser(data)
      break
    default:
      return true
  }
  userStore.emitChange()
  return true
})

export default userStore
