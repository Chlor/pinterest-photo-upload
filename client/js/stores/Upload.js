import {EventEmitter} from 'events'
import dispatcher from '../dispatcher'
import constants from '../constants/upload'
import ImageConverter from '../lib/ImageConverter'

const DATA   = new Map(),
      EVENTS = new WeakMap()

/**
 * @description private setters
 */
class Internal {
  constructor() {
    return Internal
  }

  static addImage(data) {
    DATA.set('image', data)
  }

  static setError(data) {
    DATA.set('error', data)
  }

  static setBoard(data) {
    DATA.set('board', data)
  }
}

/**
 * @description a collection of pubic getters and EventEmitter
 */
class UploadStore extends EventEmitter {
  constructor() {
    super()
  }

  emitChange() {
    this.emit('change')
  }

  getImage() {
    return DATA.get('image')
  }

  getError() {
    return DATA.get('error')
  }

  getBoard() {
    return DATA.get('board')
  }

  /**
   * @description set as unbound => bound to be able to remove event listener
   * @param fn
   * @param ctx
   */
  addChangeListener(fn, ctx) {
    EVENTS.set(fn, fn.bind(ctx))
    this.on('change', EVENTS.get(fn))
  }

  /**
   * @see #addChangeListener
   * @param fn
   */
  removeChangeListener(fn) {
    this.removeListener(EVENTS.get(fn))
    EVENTS.delete(fn)
  }
}

let uploadStore = new UploadStore()

uploadStore.dispatchToken = dispatcher.register(({action}) => {
  let {actionType, data} = action
  switch(actionType) {
    case constants.ADD_IMAGE:
      Internal.addImage(data)
      break
    case constants.CLEAR_FILE:
      Internal.addImage(null)
    case constants.ERROR:
      Internal.setError(data)
      break
    case constants.CLEAR_ERROR:
      Internal.setError(null)
      break
    case constants.SET_BOARD:
      Internal.setBoard(data)
      break
    default:
      return true
  }
  uploadStore.emitChange()
  return true
})

export default uploadStore
