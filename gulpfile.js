'use strict'

const BOOTSTRAP = './node_modules/bootstrap-css-only',
      CSS = './client/css/**/*.css',
      DIST      = './public/dist'

let gulp       = require('gulp'),
    source     = require('vinyl-source-stream'),
    browserify = require('browserify'),
    babelify   = require('babelify'),
    reactify   = require('reactify'),
    sourcemaps = require('gulp-sourcemaps'),
    watchify   = require('watchify'),
    buffer     = require('vinyl-buffer'),
    gutil      = require('gulp-util'),

    gulpFilter = require('gulp-filter'),
    gulpConcat = require('gulp-concat')

function prepareBrowserify(watch) {
  let customOpts = {
    entries: ['./client/js/app.js'],
    debug: true
  }
  let opts = Object.assign({}, watchify.args, customOpts),
      b    = browserify(opts)

  if (watch) {
    b = watchify(b)
    b.on('update', function () {
      bundle(b)
    })
  }

  b.transform([babelify.configure({
    extensions: ['.js', '.jsx'],
    experimental: true,
    optional: ['runtime'],
  }), reactify])

  b.on('log', gutil.log)

  bundle(b)
}

function bundle(b) {
  return b.bundle()
    // log errors if they happen
    .on('error', function (err) {
      error.call(this, err)
    })
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/dist/js'))
}

function error(err) {
  gutil.log(err)
  this.emit('end')
}

gulp.task('browserify', prepareBrowserify.bind(null, false))

gulp.task('watch', () => {
  prepareBrowserify(true)
  gulp.watch(CSS, ['clientCss'])
})

gulp.task('default', ['browserify'])

gulp.task('clientCss', () => {
  gulp.src(CSS)
    .pipe(gulpConcat('style.css'))
    .pipe(gulp.dest(`${DIST}/css`))
})

gulp.task('css', () =>
    gulp.src(`${BOOTSTRAP}/css/*.min.css`)
      .pipe(gulpConcat('vendor.css'))
      .pipe(gulp.dest(`${DIST}/css`))
)

gulp.task('fonts', () =>
    gulp.src(`${BOOTSTRAP}/fonts/*`)
      .pipe(gulp.dest(`${DIST}/fonts`))
)

gulp.task('build', ['css', 'clientCss', 'fonts'], prepareBrowserify.bind(null, false))
