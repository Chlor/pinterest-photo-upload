require('dotenv').load()
require('./lib/global')

let nconf = require('nconf')
nconf.argv().env()

const SSL_PORT = nconf.get('ssl_port')

let path       = require('path'),
    fs         = require('fs'),
    https      = require('https'),
    koa        = require('koa'),
    session    = require('koa-session'),
    koaStatic  = require('koa-static'),
    bodyParser = require('koa-bodyparser'),
    passport   = require('./lib/auth'),
    routes     = require('./lib/routes'),
    Api        = require('./lib/Api'),
    app        = koa()

/**
 * Since Pinterest api requires SSL, I think it`s best to use it here as well
 * @type {{key, cert}}
 */
let SSLOptions = {
  key: fs.readFileSync(nconf.get('ssl_key')),
  cert: fs.readFileSync(nconf.get('ssl_cert'))
}

app.keys = [nconf.get('secret')]
app.use(session(app))
app.use(passport.initialize())
app.use(passport.session())

app.use(bodyParser())
app.use(routes.auth)
app.use(routes.api)
app.use(Api.isAuthenticated(true))
app.use(koaStatic(path.resolve('public')))
app.use(Api.catchAll)

https.createServer(SSLOptions, app.callback())
  .listen(SSL_PORT, () => console.log(`App listening on ${SSL_PORT}`))
